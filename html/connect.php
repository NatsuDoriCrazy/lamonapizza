<?php
    $idcliente = $_POST['idcliente'];
	$nombre = $_POST['nombre'];
	$telefono = $_POST['telefono'];
	$fecha = $_POST['fecha'];
	$hora = $_POST['hora'];
	$personas = $_POST['personas'];

	// Database connection
	$conn = new mysqli('localhost','root','','monareservas');
	if($conn->connect_error){
		echo "$conn->connect_error";
		die("Connection Failed : ". $conn->connect_error);
	} else {
		$stmt = $conn->prepare("insert into reservas(idcliente, nombre, telefono, fecha, hora, personas) values( ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param("isssss", $idcliente, $nombre, $telefono, $fecha, $hora, $personas);
		$execval = $stmt->execute();
		echo $execval;
		header('Location: gracias.html');
		$stmt->close();
		$conn->close();
	}

    